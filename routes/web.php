<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\SingerController;
use App\Http\Controllers\SongController;
use App\Http\Controllers\AlbumController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('auth')->group(function () {
    Route::resource('category', CategoryController::class);
    Route::resource('singer', SingerController::class);
    Route::resource('song', SongController::class);
    Route::resource('album', AlbumController::class);
    Route::resource('users', UserController::class);
});


Route::get('/', function () {
    return view('home');
})->middleware(['auth'])->name('dashboard');
require __DIR__.'/auth.php';
