<style>
    .select2-container--default .select2-selection--multiple {
        background-color: #151719 !important;
    }
    element.style {
    /* padding: 10px; */
    height: 40px;
    line-height: 40px;
    border: 1px solid #34343e !important;
    }
    .select2-container--default .select2-selection--single {
        background-color: #151719 !important;
        margin-bottom: 50px;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        color: white !important;
    }
    .select2-selection__choice__display {
        color: #3e3e42 !important;
    }


</style>
@extends('layouts.form')
@section('page_content')

    <div class="card-body">
        <form action="{{ route('album.update', $albums->id) }}" enctype="multipart/form-data" class="row" method="post">
            @csrf
            @method('PUT')
            <div class="col-12 mb-4">
                <input type="text" class="form-control" placeholder="Albumn name" name="name" value="{{ $albums->name }}" />
            </div>
            <div class="col-12 mb-4">
                <label class="form-label">Picture</label>
                <input type="file"  name="file_picture" class="form-control" value="{{ $albums->picture }}">
            </div>
            <select  name="category_id" class="col-12 mb-4 select2-role"  aria-label="Select category">
                <option selected="selected" disabled="disabled" hidden="">Select category</option>
                @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
            <select  name="song_id" class="col-12 mt-4 select2-role" style="background-color: #151719ed;" multiple data-live-search="true">
                <option selected="selected" disabled="disabled" hidden="">Select Song</option>
                @foreach ($songs as $song)
                <option value="{{ $song->id }}">{{ $song->name }}</option>
                @endforeach
            </select>
            <div class="card-footer text-center"><button  type="submit" class="btn btn-primary" style="min-width: 140px;">Edit</button> </div>
        </form>
    </div>

@endsection
