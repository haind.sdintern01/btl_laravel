@extends('layouts.add')
@section('add')
<style>
    .select2-container--default .select2-selection--multiple {
        background-color: #151719 !important;
    }
    element.style {
    /* padding: 10px; */
    height: 40px;
    line-height: 40px;
    border: 1px solid #34343e !important;
    }
    .select2-container--default .select2-selection--single {
        background-color: #151719 !important;
        margin-bottom: 50px;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        color: white !important;
    }
    .select2-selection__choice__display {
        color: #3e3e42 !important;
    }


</style>

    <div class="card-body">
        <form style="" class="row g-3 needs-validation"
        action="{{ route('album.store') }}" method="POST"
        enctype="multipart/form-data">
        @csrf
            {{-- <div class="col-12 mb-4">
                <div class="dropzone text-center">
                    <div class="dz-message">
                        <i class="ri-upload-cloud-2-line fs-2 text-dark"></i> <span class="d-block fs-6 mt-2">Drag & Drop or click to Upload</span>
                        <span class="d-block form-text mb-4">540x320 (Max: 300KB)</span> <button type="button" class="btn btn-light-primary">Upload cover image</button>
                    </div>
                </div>
            </div> --}}

            <div class="col-12 mb-4"><input type="text" class="form-control" placeholder="Albumn name" name="name" /></div>
            <div class="col-12 mb-4">
                <label class="form-label">Picture</label>
                <input type="file"  name="file_picture" class="form-control">
            </div>



            <select  name="category_id" class="col-12 mb-4 select2-role"  aria-label="Select category">
                <option selected="selected" disabled="disabled" hidden="">Select category</option>
                @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
            <select  name="song_id[]" class="col-12 mt-4 select2-role" style="background-color: #151719ed;" multiple data-live-search="true">
                <option selected="selected" disabled="disabled" hidden="">Select Song</option>
                @foreach ($songs as $song)
                <option value="{{ $song->id }}">{{ $song->name }}</option>
                @endforeach
            </select>


            <div class="card-footer text-center"><button  type="submit" class="btn btn-primary" style="min-width: 140px;">ADD</button> </div>
        </form>
    </div>
@endsection
