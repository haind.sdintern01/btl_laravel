@extends('layouts.form')
@section('page_content')
<div class="under-hero container">
    <div class="section">
        <div class="row">
            <div class="col-xl-7 col-md-10 mx-auto">
                <div class="card">
                    <div class="card-header">
                        <h4 class="mb-0">Welcome <span class="text-primary">Profile</span></h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('users.store') }}" class="row">
                            @csrf
                            <div class="col-sm-6 mb-4">
                                <label class="form-label">Picture</label>
                                <input type="file"  name="file_picture" class="form-control">
                            </div>

                            <div class="col-sm-6 mb-4"><input type="text" class="form-control" placeholder="Họ và tên" name="name" required/></div>
                            <div class="col-sm-6 mb-4"><input type="text" class="form-control" placeholder="Email" name="email" required/></div>
                            <div class="col-sm-6 mb-4"><input type="text" class="form-control" placeholder="Tài khoản" name="username" required/></div>
                            <div class="col-sm-6 mb-4"><input type="password" class="form-control" placeholder="Mật khẩu" name="password" required /></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
