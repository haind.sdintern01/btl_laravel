@extends('layouts.form')
@section('page_content')
<div class="under-hero container">
    <div class="section">
        <div class="section__head">
            <h3 class="mb-0">Featured <span class="text-primary">Artists</span></h3>
        </div>
        <div class="swiper-carousel swiper-carousel-button">
            <div class="swiper" data-swiper-slides="6" data-swiper-autoplay="true">
                <div class="swiper-wrapper">
                    @foreach ($singers as $singer)
                    <div class="swiper-slide">
                        <div class="avatar avatar--xxl d-block text-center">
                            <div class="avatar__image">
                                <a href="artist-details.html"><img src="{{ $singer->picture }}" /></a>
                            </div>
                            <a href="artist-details.html" class="avatar__title mt-3">{{ $singer->name }}</a>

                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="section__head">
            <h3 class="mb-0">Top <span class="text-primary">Artists</span></h3>
        </div>
        <div class="row g-4">
            @foreach ($singers as $singer)
            <div class="col-6 col-xl-2 col-md-3 col-sm-4">
                <a href="artist-details.html" class="cover cover--round">
                    <div class="cover__image"><img style="width: " src="{{ $singer->picture }}" alt="Arebica Luna" /></div>
                    <div class="cover__foot"><span class="cover__title text-truncate">{{ $singer->name }}</span></div>
                </a>
                <a href="{{ route('singer.edit', $singer->id) }}">
                    <button type='submit' class='btn bg-yellow    btn-flat'  name=''>Sửa</button>
                </a>
                <form action="{{ route('singer.destroy', $singer->id) }}" method="POST" onclick="return confirm('Are you sure?')">
                    @csrf
                    @method('DELETE')
                    <button type='submit' class='btn bg-red btn-flat'>Xóa</button></a>
                </form>
            </div>
            @endforeach
        <div class="mt-5 text-center">
            <a href="javascript:void(0);" class="btn btn-primary">
                <div class="btn__wrap"><i class="far fa-undo"></i> <span>Load more</span></div>
            </a>
        </div>
    </div>
</div>
@endsection
