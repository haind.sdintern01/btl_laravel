@extends('layouts.add')
@section('add')
    <div class="card-body">
        <form style="" class="row g-3 needs-validation"
        action="{{ route('song.store') }}" method="POST"
        enctype="multipart/form-data">
        @csrf
            {{-- <div class="col-12 mb-4">
                <div class="dropzone text-center">
                    <div class="dz-message">
                        <i class="ri-upload-cloud-2-line fs-2 text-dark"></i> <span class="d-block fs-6 mt-2">Drag & Drop or click to Upload</span>
                        <span class="d-block form-text mb-4">540x320 (Max: 300KB)</span> <button type="button" class="btn btn-light-primary">Upload cover image</button>
                    </div>
                </div>
            </div> --}}

            <div class="col-12 mb-4"><input type="text" class="form-control" placeholder="Song name" name="name" /></div>
            <div class="col-sm-6 mb-4">
                <label class="form-label">Picture</label>
                <input type="file"  name="file_picture" class="form-control">
            </div>

            <div class="col-sm-6 mb-4">
                <label class="form-label">Song file</label>
                <input type="file"  name="file_song" class="form-control">
            </div>

            <select  name="category_id" class="col-12 mb-4" style="background-color: #151719ed;" aria-label="Select category">
                <option selected="selected" disabled="disabled" hidden="">Select category</option>
                @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
            <select name="singer_id" class="form-select" style="background-color: #151719ed;" aria-label="Select category"><option selected="selected" disabled="disabled" hidden="">Select singer</option>
                @foreach ($singers as $singer)
                <option value="{{ $singer->id }}">{{ $singer->name }}</option>
                @endforeach
            </select>
            <div class="card-footer text-center"><button  type="submit" class="btn btn-primary" style="min-width: 140px;">ADD</button> </div>
        </form>
    </div>
@endsection
