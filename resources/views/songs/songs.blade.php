@extends('layouts.form')
@section('page_content')
<div class="under-hero container">
    <div class="section">


    </div>
    <div class="section">
        <div class="section__head">
            <div class="flex-grow-1">
                <h3 class="mb-0">Free <span class="text-primary">Music</span></h3>
            </div>

        </div>
        <div class="list">
            <div class="row">
                @foreach ($songs as $song)
                <div class="col-xl-6">
                    <div class="list__item">
                        <div class="list__cover">
                            <img src="{{ $song->picture }}" alt="Hình" />
                            <a href="javascript:void(0);" class="btn btn-play btn-sm btn-default btn-icon rounded-pill" data-play-id="{{ $song->id }}" aria-label="Play pause">
                                <i class="fal fa-play"></i> <i class="fal fa-pause"></i>
                            </a>
                        </div>
                        <div class="list__content">
                            <a href="#" class="list__title text-truncate">{{ $song->name }}</a>
                            <p class="list__subtitle text-truncate"><a href="#">{{ $song->singers->name }}</a></p>
                        </div>
                        <ul class="list__option">
                            <li>
                                <a href="javascript:void(0);" role="button" class="d-inline-flex" aria-label="Favorite" data-favorite-id="1">
                                    <i class=""></i> <i class="ri-heart-fill heart-fill"></i>
                                </a>
                            </li>

                            <li class="dropstart d-inline-flex">
                                <a class="dropdown-link" href="javascript:void(0);" role="button" data-bs-toggle="dropdown" aria-label="Cover options" aria-expanded="false"><i class="fas fa-ellipsis-h-alt"></i></a>
                                <ul class="dropdown-menu dropdown-menu-sm">
                                    <li><a class="dropdown-item" href="javascript:void(0);" role="button" data-play-id="{{ $song->path }}">Play</a></li>
                                    <li>
                                        <a href="{{ route('song.edit', $song->id) }}">
                                        <button type='submit' class='btn bg-yellow    btn-flat'  name=''>Sửa</button>
                                        </a>
                                        <form action="{{ route('song.destroy', $song->id) }}" method="POST" onclick="return confirm('Are you sure?')">
                                        @csrf
                                        @method('DELETE')
                                        <button type='submit' class='btn bg-red btn-flat'>Xóa</button></a>
                                        </form>
                                    </li>
                                </ul>



                            </li>
                        </ul>
                    </div>

                </div>
                @endforeach
            </div>
        </div>
        <div class="mt-5 text-center" style="margin-right:50%">

                   {{ $songs->links()  }}


        </div>
    </div>
</div>
@endsection
