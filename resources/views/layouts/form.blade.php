<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <meta name="description" content="Listen App - Online Music Streaming App Template" />
        <meta name="keywords" content="music template, music app, music web app, responsive music app, music, themeforest, html music app template, css3, html5" />
        <title>Listen App - Online Music Streaming App</title>
        <link href="https://www.kri8thm.in/html/listen/theme/demo/images/logos/favicon.png" rel="icon" />
        <link rel="apple-touch-icon" href="https://www.kri8thm.in/html/listen/theme/demo/images/logos/touch-icon-iphone.png" />
        <link rel="apple-touch-icon" sizes="152x152" href="https://www.kri8thm.in/html/listen/theme/demo/images/logos/touch-icon-ipad.png" />
        <link rel="apple-touch-icon" sizes="180x180" href="https://www.kri8thm.in/html/listen/theme/demo/images/logos/touch-icon-iphone-retina.png" />
        <link rel="apple-touch-icon" sizes="167x167" href="https://www.kri8thm.in/html/listen/theme/demo/images/logos/touch-icon-ipad-retina.png" />
        <link href="https://www.kri8thm.in/html/listen/theme/demo/css/plugins.bundle.css" rel="stylesheet" type="text/css" />
        <link href="https://www.kri8thm.in/html/listen/theme/demo/css/styles.bundle.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
        <link rel="import" href="https://rawgit.com/sitepoint-editors/multiselect-web-component/master/src/multiselect.html">
        <link rel="stylesheet" href="{{ asset('user/select2.min.css') }}">
        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="stylesheet" type="text/css" href="{{ asset('fontawesome/css/all.css') }}">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700;800&display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet" />
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .cover__image img {
                width: 275px !important;
                height: 206px !important;
                object-fit: cover;
            }
        </style>
    </head>
    <body>
        <div id="line_loader"></div>
        <div id="loader">
            <div class="loader">
                <div class="loader__eq mx-auto"><span></span> <span></span> <span></span> <span></span> <span></span> <span></span></div>
                <span class="loader__text mt-2">Loading</span>
            </div>
        </div>
        <div id="wrapper">
            @include('layouts.sidebar')
            @include('layouts.header')
            <main id="page_content">
                <div class="hero" style="background-image: url(https://www.kri8thm.in/html/listen/theme/demo/images/banner/home.jpg);"></div>
                @yield('page_content')
            @include('layouts.footer')
            </main>
        </div>

        @include('layouts.play')
        <div id="backdrop"></div>
        <script src="{{ asset('listen/plugins.bundle.js') }}"></script>

        <script src="{{ asset('user/select2.min.js') }}"></script>
        <script src="{{ asset('user/user.js') }}"></script>
        <script src="{{ asset('listen/scripts.bundle.js') }}"></script>
    </body>
</html>
