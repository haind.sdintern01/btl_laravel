<aside id="sidebar">
    <div class="sidebar-head d-flex align-items-center justify-content-between">
        <a href="index.html" class="brand external"><img src="https://www.kri8thm.in/html/listen/theme/demo/images/logos/logo.svg" alt="Listen app" /> </a>
        <a href="javascript:void(0);" role="button" class="sidebar-toggler" aria-label="Sidebar toggler">
            <div class="d-none d-lg-block"><i class="fad fa-bars"></i></div>
            <i class="ri-menu-fold-line d-lg-none"></i>
        </a>
    </div>
    <div class="sidebar-body" data-scroll="true">
        <nav class="navbar d-block p-0">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="{{ route('dashboard') }}" class="nav-link d-flex align-items-center active"><i class="fas fa-home"></i> <span class="ps-3">Home</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('category.index') }}" class="nav-link d-flex align-items-center"><i class="far fa-compact-disc"></i><span class="ps-3">Categories</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('song.index') }}" class="nav-link d-flex align-items-center"><i class="fas fa-music"></i><span class="ps-3">Songs</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('album.index') }}" class="nav-link d-flex align-items-center"><i class="fal fa-album-collection"></i> <span class="ps-3">Albums</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('singer.index') }}" class="nav-link d-flex align-items-center"><i class="fas fa-user-music"></i> <span class="ps-3">Singers</span></a>
                </li>

                <li class="nav-item nav-item--head"><span class="nav-item--head__text">Music</span> <span class="nav-item--head__dots">...</span></li>
                <li class="nav-item">
                    <a href="" class="nav-link d-flex align-items-center"><i class="fad fa-chart-pie-alt"></i> <span class="ps-3">Analytics</span></a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link d-flex align-items-center"><i class="far fa-heart"></i> <span class="ps-3">Favorites</span></a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link d-flex align-items-center"><i class="fal fa-history"></i> <span class="ps-3">History</span></a>
                </li>

            </ul>
        </nav>
    </div>
    <div class="sidebar-foot">
        <a href="{{ route('song.create') }}" class="btn btn-primary d-flex">
            <div class="btn__wrap"><i class="fas fa-music-alt"></i> <span>Add Music</span></div>
        </a>
    </div>
</aside>
