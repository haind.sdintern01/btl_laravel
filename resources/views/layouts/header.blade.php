<header id="header">
    <div class="container">
        <div class="header-container">
            <div class="d-flex align-items-center">
                <a href="javascript:void(0);" role="button" class="header-text sidebar-toggler d-lg-none me-3" aria-label="Sidebar toggler"><i class="ri-menu-3-line"></i></a>
                <form action="#" id="search_form" class="me-3">
                    <label for="search_input"><i class="fal fa-search"></i></label> <input type="text" placeholder="Type anything to get result..." id="search_input" class="form-control form-control-sm" />
                </form>
                <div id="search_results" class="search pb-3">


                </div>
                <div class="d-flex align-items-center">
                    <div class="dropdown">

                    </div>
                    <div class="dropdown ms-3 ms-sm-4">

                        <a href="javascript:void(0);" class="avatar header-text" role="button" id="user_menu" data-bs-toggle="dropdown" aria-expanded="false">
                            <div class="avatar__image"><img src="https://www.kri8thm.in/html/listen/theme/demo/images/users/thumb.jpg" alt="user" /></div>
                            <span class="ps-2 d-none d-sm-block">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-md dropdown-menu-end" aria-labelledby="user_menu">
                            <li>
                                <div class="py-2 px-3 avatar avatar--lg">
                                    <div class="avatar__image"><img src="https://www.kri8thm.in/html/listen/theme/demo/images/users/thumb.jpg" alt="user" /></div>
                                    <div class="avatar__content"><span class="avatar__title">{{ Auth::user()->name }}</span> <span class="avatar__subtitle"></span></div>
                                </div>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li>
                                <a class="dropdown-item d-flex align-items-center" href="{{ route('users.create') }}"><i class="ri-user-3-line fs-5"></i> <span class="ps-2">Profile</span></a>
                            </li>

                            <li class="dropdown-divider"></li>
                            <li>
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf
                                    <button class="ps-2">Logout</button>
                                </form>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
