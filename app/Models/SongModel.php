<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SongModel extends Model
{
    use HasFactory;
    protected $table = 'songs';
    protected $fillable = ['name', 'picture', 'count', 'singer_id', 'path'];
    public function categories()
    {
        return $this->belongsToMany(CategoryModel::class, 'category_song', 'song_id', 'category_id');
    }
    public function albums()
    {
        return $this->belongsToMany(AlbumModel::class, 'album_song', 'song_id', 'album_id');
    }
    public function singers()
    {
        return $this->belongsTo(SingerModel::class, 'singer_id', 'id');
    }
}
