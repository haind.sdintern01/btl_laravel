<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategorySongModel extends Model
{
    use HasFactory;
    protected $table = "category_song";
    protected $fillable = ['song_id', 'category_id'];
}
