<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SongModel;
use App\Models\SingerModel;
use App\Models\CategoryModel;
use App\Models\AlbumModel;
use App\Models\CategorySongModel;
use App\Http\Requests\SongRequest;
use App\Models\AlbumSongModel;

class SongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $songs = SongModel::with('categories', 'singers', 'albums')->paginate(6);
        return view('songs.songs',[
            'songs'=>$songs,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $songs = SongModel::all();
        $albums = AlbumModel::all();
        $categories = CategoryModel::all();
        $singers = SingerModel::all();
        return view('songs.add', [
            'songs'=>$songs,
            'albums'=>$albums,
            'categories'=>$categories,
            'singers'=>$singers,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SongRequest $request)
    {
        $songs = SongModel::create([
            'name' => $request->input('name'),
            'singer_id'=>$request->input('singer_id'),
            'picture' => 'song_picture/'.$request->file_picture->getClientOriginalName(),
            'path' => 'songs/'.$request->file_song->getClientOriginalName(),
        ]);
        CategorySongModel::create([
            'song_id' => $songs->id,
            'category_id' => $request->category_id,
        ]);
        $songs->save();
        $request->file_picture->move("song_picture/", $request->file_picture->getClientOriginalName());
        $request->file_song->move("songs/", $request->file_song->getClientOriginalName());

        return redirect()->route('song.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $songs = SongModel::find($id);
        $songs->load('categories', 'singers', 'albums');
        $categories = CategoryModel::all();
        $singers = SingerModel::all();
        $albums = AlbumModel::all();
        return view('songs.edit', [
            'albums'=> $albums,
            'categories'=> $categories,
            'singers'=> $singers,
            'songs'=> $songs,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SongRequest $request, $id)
    {

        $songs = SongModel::where('id', $id)
            ->update([
                'name' => $request->input('name'),
                'singer_id'=>$request->input('singer_id'),
                'picture' => 'song_picture/'.$request->file_picture->getClientOriginalName(),
                'path' => 'songs/'.$request->file_song->getClientOriginalName(),
            ]);
        $cate_song = CategorySongModel::where('song_id', $id)
            ->update([
            'song_id' => $id,
            'category_id' => $request->category_id[0],
        ]);

        $request->file_picture->move("song_picture/", $request->file_picture->getClientOriginalName());
        $request->file_song->move("songs/", $request->file_song->getClientOriginalName());
        return redirect()->route('song.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $songs = SongModel::find($id);
        $songs->delete();
        return redirect()->route('song.index');
    }
}
