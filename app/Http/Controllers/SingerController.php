<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SingerModel;
use App\Http\Requests\SingerRequest;

class SingerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $singers = SingerModel::all();
        return view('singers.singers',[
            'singers'=>$singers,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('singers.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SingerRequest $request)
    {
        $singers = SingerModel::create([
            'name' => $request->input('name'),
            'picture' => 'singers/'.$request->file->getClientOriginalName(),
        ]);
        $singers->save();
        $request->file->move("singers/", $request->file->getClientOriginalName());

        return redirect()->route('singer.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $singers = SingerModel::find($id);
        return view('singers.edit', [
            'singers'=> $singers,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SingerRequest $request, $id)
    {
        $singers = SingerModel::where('id', $id)
            ->update([
                'name'=>$request->input('name'),
                'picture' => 'singers/'.$request->file->getClientOriginalName(),
            ]);
            $request->file->move("singers/", $request->file->getClientOriginalName());
        return redirect()->route('singer.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $singers = SingerModel::find($id);
        $singers->delete();
        return redirect()->route('singer.index');
    }
}
