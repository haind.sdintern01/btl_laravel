<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SongModel;
use App\Models\SingerModel;
use App\Models\CategoryModel;
use App\Models\AlbumModel;
use App\Models\AlbumSongModel;
use App\Models\AlbumCategoryModel;
use App\Http\Requests\AlbumRequest;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $albums = AlbumModel::with('categories', 'singers', 'songs')->paginate(10);
        return view('albums.albums',[
            'albums'=>$albums,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $songs = SongModel::all();
        $albums = AlbumModel::all();
        $categories = CategoryModel::all();
        $singers = SingerModel::all();
        return view('albums.add', [
            'songs'=>$songs,
            'albums'=>$albums,
            'categories'=>$categories,
            'singers'=>$singers,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AlbumRequest $request)
    {

        $albums = AlbumModel::create([
            'name' => $request->input('name'),
            'picture' => 'album_picture/'.$request->file_picture->getClientOriginalName(),

        ]);

        foreach($request->song_id as $song){
            AlbumSongModel::create([
                'album_id' => $albums->id,
                'song_id' => $song,
            ]);
        }



        AlbumCategoryModel::create([
            'album_id' => $albums->id,
            'category_id' => $request->category_id,
        ]);
        $albums->save();
        $request->file_picture->move("album_picture/", $request->file_picture->getClientOriginalName());
        return redirect()->route('album.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $albums = AlbumModel::find($id);
        $albums->load('categories', 'singers', 'songs');
        $categories = CategoryModel::all();
        $singers = SingerModel::all();
        $songs = SongModel::all();
        return view('albums.edit', [
            'albums'=> $albums,
            'categories'=> $categories,
            'singers'=> $singers,
            'songs'=> $songs,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AlbumRequest $request, $id)
    {
        $albums = AlbumModel::where('id', $id)
            ->update([
            'name' => $request->input('name'),
            'picture' => 'album_picture/'.$request->file_picture->getClientOriginalName(),
            ]);
        $album_song = AlbumSongModel::where('album_id', $id)
            ->update([
            'album_id' => $id,
            'song_id' => $request->song_id,
        ]);
        $album_cate = AlbumCategoryModel::where('album_id', $id)
            ->update([
            'album_id' => $id,
            'category_id' => $request->category_id,
        ]);
        $request->file_picture->move("album_picture/", $request->file_picture->getClientOriginalName());
        return redirect()->route('album.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $albums = AlbumModel::find($id);
        $albums->delete();
        return redirect()->route('album.index');
    }
}
